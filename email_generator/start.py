from argparse import ArgumentParser
from os.path import join, abspath, dirname, exists, isfile
import json
try:
    from email_generator.body.email_body import EmailBody
    from email_generator.utils import utils
except:
    from .body.email_body import EmailBody
    from .utils import utils


def get_parser():
    parser = ArgumentParser()
    parser.add_argument('-d', '--dest_file', dest='dest_file', default=None, type=str,
                        help='Location of the csv with email destinations')
    parser.add_argument('-m', '--my_info', dest='my_info', default=None, type=str,
                        help='JSON file with my information in it. '
                             'Keys are as follows: {myname: str, company_name: str, my_pos: str, my_services: [str...]}')
    arguments = parser.parse_args()
    dest_csv = arguments.dest_file
    my_info_json = arguments.my_info
    if dest_csv is None:
        print("No destination information given.")
        exit()
    if my_info_json is None:
        print("No information of sender is given.")
        exit()
    if not exists(dest_csv):
        print("Destination Information csv does not exist.")
        exit()
    if not exists(my_info_json):
        print("My information json does not exist.")
        exit()
    if not isfile(dest_csv):
        print("Destination clients csv does not exist.")
        exit()
    if not isfile(my_info_json):
        print("My information json does not exist.")
        exit()
    return dest_csv, my_info_json



def parse_json_file(json_file):
    my_data = json.load(open(json_file, 'r'))
    keys = my_data.key()
    if 'name' not in keys:
        exit()
    if 'company' not in keys:
        exit()
    if 'position' not in keys:
        exit()
    if 'services' not in keys:
        exit()
    return my_data


def get_key(dc, k):
    try:
        data = dc[k]
    except:
        data = None
    return data


def parse_dest_client_file(csv_file):
    dest_information = []
    with open(csv_file, 'r') as csv_data_file:
        for client_info_line in csv_data_file.readlines():
            info = {}
            client_info = client_info_line.replace('\n', '').split(',')
            client_data_len = len(client_info)
            info['name'] = client_info[0]
            info['company'] = client_info[1]
            info['position'] = client_info[2]
            if client_data_len > 3:
                info['services'] = client_info[3:]
            dest_information.append(info)
    return dest_information


def generate_emails(my_info, clients):
    total_clients = len(clients)
    datetime = "in the next week"
    my_name = my_info['name']
    my_company = my_info['company']
    my_position = my_info['position']
    my_services = my_info['services']
    for client in clients:
        client_name = get_key(client, 'name')
        client_company = get_key(client, 'company')
        client_position = get_key(client, 'position')
        client_services = get_key(client, 'services')
        email_body = EmailBody(my_name, my_company, my_position, my_services,
                               client_name, client_company, client_position, client_services,
                               None, datetime)
        mail = email_body.generate_email()
        print(mail)

def initialize_email_generation():
    dest_csv_file, my_info_file = get_parser()
    my_info = parse_json_file(my_info_file)
    clients = parse_dest_client_file(dest_csv_file)


def start():
    their_name = "Johannson Milich"
    my_name = "Mudassir"
    their_company = "Vision Genius"
    my_company = "Think Transportation"
    their_services = ["Person Detection", "Person Authentication", "Gender Detection", "Face Detection",
                    "Face Identification", "Age Group", "Facial Expression", "Weapon Detection", "Posture Detection",
                    "Vehicle Detection", "Vehicle Classification", "Anomaly Detection", "Object Detection",
                    "Object Tracking", "Product Classification", "Logo Detection"]
    our_services = ["Transportation Planning", "Traffic Engineering", "Traffic Impact Studies",
                      "Transportation Data Analytics", "Autonomous Vehicles", "Smart Cities", "Railway Engineering",
                      "Airports", "Sea Ports", "Highway Engineering and Design", "Logistics",
                      "Public Transport Planning and Design", "Traffic Survey", "Data Collection",
                      "GIS Mapping and Remote Sensing"]
    their_position = "CEO"
    my_position = "CMO"
    datetime = "11/10/2020 05:00 UTC"
    our_services_details = None
    email_body = EmailBody(my_name, my_company, my_position, our_services,
                           their_name, their_company, their_position, their_services,
                           our_services_details, datetime)
    mail = email_body.generate_email()
    print(mail)


def test_choose_options():
    text = "{Hello|Hi|Greetings|How are you|Howdy} Anthony! {What is up?|How is it going?|It is good that you are well.}"
    print(utils.choose_options(text))


if __name__ == "__main__":
    start()
