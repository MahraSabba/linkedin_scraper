import smtplib
import csv
import random
import time
import pandas as pd
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart



"""
    1. The limit of the emails sent/day will be 200.
    2. The ranking of the emails being sent to company X will be as follows:
        1. CEO
        2. The rest
        with a maximum of 5 people per company.
"""


def initialize_smtp_client(my_email="adil@thinktransportation.net", password='XIhwGWbJDX7b'):
    server = smtplib.SMTP('smtp.zoho.com', 587)
    server.starttls()
    server.login(my_email, password)
    return server


def send_one_email(smtp_client, email_from, client_info, subject='Traffic Consultant | Think Transportation'):
    receiver_email = client_info['Email']
    have_sent = client_info['Email_Sent']
    email_generated = client_info['Content_Generated']
    email_verified = client_info['Email_Verified']

    # if not email_verified:
    #     return (False, client_info, "Not Verified")
    if have_sent:
        return (False, client_info, "Already Sent")
    if type(receiver_email) is list:
        if receiver_email is []:
            return (False, client_info, "Client Email List is Empty")
        else:
            receiver_email = receiver_email[0]
    if type(receiver_email) is str:
        if receiver_email == "":
            return (False, client_info, "Client Email String is Empty")
    if type(email_generated) is bool:
        return (False, client_info, "Email has not been generated")
    if email_generated == "":
        return (False, client_info, "Email is not generated for this client")
    email_generated = email_generated.replace('"', '')
    msg = MIMEMultipart()
    msg['From'] = email_from
    msg['Subject'] = subject
    msg['To'] = receiver_email
    msg.attach(MIMEText(email_generated, 'html'))
    msg_text = msg.as_string()
    print(msg_text)
    try:
        smtp_client.sendmail(email_from, receiver_email, msg_text)
    except smtplib.SMTPRecipientsRefused as exc:
        print(exc)
        return (False, client_info, "Not a single client received the email.")
    except smtplib.SMTPHeloError as exc:
        print(exc)
        return (False, client_info, "SMTP Server did not respond to the HELO greeting.")
    except smtplib.SMTPSenderRefused as exc:
        print(exc)
        return (False, client_info, f"SMTP Server did not accept the MY/FROM address i.e: {email_from}")
    except smtplib.SMTPDataError as exc:
        print(exc)
        return (False, client_info, "SMTP Server returned an unexpected error.")
    except smtplib.SMTPNotSupportedError as exc:
        print(exc)
        return (False, client_info, "SMTP Server not supported error.")

    client_info['Email_Sent'] = True
    return (True, client_info, True)


def send_multiple_emails(smtp_server, my_email, clients, subject='Traffic Consultant | Think Transportation'):
    for index, client in clients.iterrows():
        sent, client_sent_info, error = send_one_email(smtp_client=smtp_server,
                                                       email_from=my_email,
                                                       client_info=client,
                                                       subject=subject)


def send_email(mail_list, template, subject):

    # mail_list = []
    amount = []
    name = []
    email_from = "adil@thinktransportation.net"
    password = 'XIhwGWbJDX7b'

    for mail_to in mail_list:
        send_to_email = mail_to
        find_des = mail_list.index(send_to_email)
        clientName = ''

        # subject = f'{clientName} Think Transportation | Company Introduction'
        message = template
        subject = "Traffic Consultant | Think Transportation"
        msg = MIMEMultipart()
        msg['From'] = email_from
        msg['Subject'] = subject
        msg['To'] = send_to_email
        # print(send_to_email)
        msg.attach(MIMEText(message, 'html'))
        # text = msg
        text = msg.as_string()

        # is_valid = validte_email(send_to_email)
        print(send_to_email)
        # print('%s is valid %s'%(send_to_email,str(is_valid)))


        # server.sendmail(email_from, send_to_email, text)
        # print("email sent to %s out of %s / %s emails"%(send_to_email,str(mail_list.index(mail_to)),str(len(mail_list))))
        # time.sleep(random.randint(15, 30))


    # server.quit()
    print('Process is finished!')
    # exit()
    # time.sleep(10)
