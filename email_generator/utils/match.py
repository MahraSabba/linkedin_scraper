def get_top_k_matches(matching_list, top_k=4):
    our_services = set()
    their_services = set()
    for (our_service, their_service, matching_value) in matching_list:
        if matching_value < 0.4:
            continue
        if len(our_services) < top_k:
            our_services.add(our_service)
        if len(their_services) < top_k:
            their_services.add(their_service)
    return list(our_services), list(their_services)
