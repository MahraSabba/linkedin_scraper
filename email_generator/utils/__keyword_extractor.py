from os.path import abspath, join, dirname
from argparse import ArgumentParser

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from nltk.tokenize import word_tokenize


class KeywordExtractor(object):

    def __init__(self):
        self.file, self.datatype, self.top_n_keywords = self.start_argparse()
        self.keywords = None
        self.counter = None
        self.tfidf_transformer = None
        self.word_count_vector = None
        self.feature_names = None
        self.count_information = None
        self.information = []
        self.saving_location = self.file.replace('.csv', '___result.csv')

    @staticmethod
    def start_argparse():
        argparser = ArgumentParser()
        argparser.add_argument('-f', '--file', type=str, dest='file',
                               default="E:\\Python Application\\email_generator\\sample\\words-list.csv",
                               help='The location of the csv file.')
        argparser.add_argument('-t', '--type', type=str, dest='type',
                               default='word',
                               help='The type of data in the file. {word|sentence}')
        argparser.add_argument('-k', '--keywordcount', type=int, dest='keywordcount',
                               default=100,
                               help='Enter a number with the maximum number of keywords to extract. Default is 200.')
        arguments = argparser.parse_args()
        file = arguments.file
        datatype = arguments.type
        keyword_count = arguments.keywordcount
        if file is None:
            print(f'Enter the location of the file you want to analyze in this form\n'
                  f'--file="C:/data/file.csv" after the name of the file {abspath(__file__)}.')
            exit()
        return file, datatype, keyword_count

    @staticmethod
    def go_through_file(datafile):
        for line in datafile.readlines():
            line = line.replace('\n', '').lower()
            yield line

    @staticmethod
    def parse_word(word):
        return word

    @staticmethod
    def parse_sentence(sentence):
        tokenized_line = word_tokenize(sentence)
        return tokenized_line

    @staticmethod
    def sort_coo(coo_matrix):
        tuples = zip(coo_matrix.col, coo_matrix.data)
        return sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)

    @staticmethod
    def extract_topn_from_vector(feature_names, sorted_items, topn=10):
        """get the feature names and tf-idf score of top n items"""

        # use only topn items from vector
        sorted_items = sorted_items[:topn]

        score_vals = []
        feature_vals = []

        for idx, score in sorted_items:
            fname = feature_names[idx]

            # keep track of feature name and its corresponding score
            score_vals.append(round(score, 3))
            feature_vals.append(feature_names[idx])

        # create a tuples of feature,score
        # results = zip(feature_vals,score_vals)
        results = {}
        for idx in range(len(feature_vals)):
            results[feature_vals[idx]] = score_vals[idx]

        return results

    def analyze_file(self):
        self.information = []
        with open(self.file, 'r') as datafile:
            if self.datatype == 'word':
                for line in self.go_through_file(datafile):
                    self.information.append(self.parse_word(line))
            elif self.datatype == 'sentence':
                for line in self.go_through_file(datafile):
                    self.information.extend(self.parse_sentence(line))
        self.counter = CountVectorizer(stop_words='english', max_df=0.85, max_features=1000)
        self.word_count_vector = self.counter.fit_transform(self.information)
        self.feature_names = self.counter.get_feature_names()
        print(self.word_count_vector.shape)
        self.tfidf_transformer = TfidfTransformer(smooth_idf=True, use_idf=True)
        self.tfidf_transformer.fit(self.word_count_vector)

    def analyze_keywords(self):
        tf_idf_vector = self.tfidf_transformer.transform(self.counter.transform(self.information))
        sorted_items = self.sort_coo(tf_idf_vector.tocoo())
        keywords = self.extract_topn_from_vector(self.feature_names, sorted_items, self.top_n_keywords)
        self.keywords = keywords

    def print_keywords(self):
        for word_id, word in enumerate(self.keywords):
            print(word_id, word)

    def save_keywords(self):
        with open(self.saving_location, 'w') as saving_file:
            for keyword in self.keywords:
                saving_file.write(f'{keyword}\n')


if __name__ == "__main__":
    keyword_extractor = KeywordExtractor()
    keyword_extractor.analyze_file()
    keyword_extractor.analyze_keywords()
    keyword_extractor.print_keywords()
    keyword_extractor.save_keywords()

