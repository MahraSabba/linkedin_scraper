import pandas as pd


def filter_dataframe(dataframe):
    return dataframe[(dataframe['Is_Scraped'] == True) &
                     (dataframe['Email_Verified'] == False) &
                     (pd.isna(dataframe['Email']) == False)]


def get_scraped_not_sent_people(people_df):
    return filter_dataframe(people_df)


def set_verified_email(people_df, index, emails):
    email_index = people_df.columns.get_loc('Email')
    email_verified_index = people_df.columns.get_loc('Email_Verified')
    people_df.iat[index, email_index] = emails
    people_df.iat[index, email_verified_index] = True
    return people_df


def start():
    from os.path import dirname, abspath, join

    data_loc = join(abspath(dirname(__file__)), "merger-civil-arham-sample.csv")
    dataframe = pd.read_csv(data_loc)
    people = get_scraped_not_sent_people(dataframe)
    for index, row in people.iterrows():
        emails = row['Email']
        dataframe = set_verified_email(dataframe, index, emails)
    data_loc_new = join(abspath(dirname(__file__)), "merger-civil-arham-sample-new.csv")
    dataframe.to_csv(data_loc_new, index=False)


if __name__ == '__main__':
    start()
