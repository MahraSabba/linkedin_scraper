from os.path import abspath
from argparse import ArgumentParser
from tqdm import tqdm
from collections import Counter
from string import punctuation
from nltk.tokenize import word_tokenize
from spellchecker import SpellChecker


class KeywordExtractor(object):

    def __init__(self):
        self.file, self.datatype, self.top_n_keywords = self.start_argparse()
        self.keywords = None
        self.counter = None
        self.tfidf_transformer = None
        self.word_count_vector = None
        self.feature_names = None
        self.count_information = None
        self.information = []
        self.spell = SpellChecker()
        self.saving_location = self.file.replace('.csv', '_simple_result.csv')

    @staticmethod
    def start_argparse():
        argparser = ArgumentParser()
        argparser.add_argument('-f', '--file', type=str, dest='file',
                               default="E:\\Python Application\\email_generator\\sample\\words-list.csv",
                               help='The location of the csv file.')
        argparser.add_argument('-t', '--type', type=str, dest='type',
                               default='word',
                               help='The type of data in the file. {word|sentence}')
        argparser.add_argument('-k', '--keywordcount', type=int, dest='keywordcount',
                               default=1000,
                               help='Enter a number with the maximum number of keywords to extract. Default is 200.')
        arguments = argparser.parse_args()
        file = arguments.file
        datatype = arguments.type
        keyword_count = arguments.keywordcount
        if file is None:
            print(f'Enter the location of the file you want to analyze in this form\n'
                  f'--file="C:/data/file.csv" after the name of the file {abspath(__file__)}.')
            exit()
        return file, datatype, keyword_count

    @staticmethod
    def go_through_file(datafile):
        for line in datafile.readlines():
            line = line.replace('\n', '').lower()
            yield line

    def parse_word(self, word):
        token = self.analyze_token(word)
        return token

    def analyze_token(self, token):
        if len(self.spell.unknown([token])) == 1:
            return None
        if token == ' ':
            return None
        if token in punctuation:
            return None
        if any(map(str.isdigit, token)):
            return None
        if len(token) >= 15 and " " not in token:
            return None
        return token

    def parse_sentence(self, sentence):
        result = []
        for word in word_tokenize(sentence):
            token = self.analyze_token(word)
            if token is not None:
                result.append(token)
        print(result)
        return result

    @staticmethod
    def reverse_dict(count_info):
        return {k: v for k, v in sorted(count_info.items(),
                                        key=lambda item: item[1])}

    def analyze_file(self):
        with open(self.file, 'r') as datafile:
            if self.datatype == 'word':
                for line in tqdm(self.go_through_file(datafile)):
                    word = self.parse_word(line)
                    if word is not None:
                        self.information.append(word)
            elif self.datatype == 'sentence':
                for line in tqdm(self.go_through_file(datafile)):
                    self.information.extend(self.parse_sentence(line))
        print(self.information)
        self.count_information = self.reverse_dict(Counter(self.information))

    def parse_keywords(self):
        keywords = []
        for count_id, (word, count) in enumerate(self.count_information.items()):
            if count_id > self.top_n_keywords:
                break
            keywords.append((word, count))
        self.keywords = keywords

    def get_keywords(self):
        return self.keywords

    def print_keywords(self):
        for word_id, word in enumerate(self.keywords):
            print(word_id, word)

    def save_keywords(self):
        with open(self.saving_location, 'w') as saving_file:
            for keyword, count in self.count_information.items():
                saving_file.write(f'{keyword}, {count}\n')


if __name__ == "__main__":
    keyword_extractor = KeywordExtractor()
    keyword_extractor.analyze_file()
    keyword_extractor.parse_keywords()
    keyword_extractor.print_keywords()
    keyword_extractor.save_keywords()
