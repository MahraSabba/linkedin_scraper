from nltk.corpus import wordnet as wn
from itertools import product
import spacy
import re
from random import randint


def choose_options(text):
    matches = re.findall('\{[\w|,\-?!\. ]+\}', text)
    for match in matches:
        cur_values = match.replace('{', '').replace('}', '').split('|')
        cur_value = cur_values[randint(0, len(cur_values) - 1)]
        text = text.replace(match, cur_value)
    return text


def similarity(list1, list2):
    nlp = spacy.load('en_core_web_lg')

    l1_tokens = []
    l2_tokens = []
    for lv1 in list1:
        l1_tokens.append(nlp(lv1))

    for lv2 in list2:
        l2_tokens.append(nlp(lv2))

    sim_score = []
    for l1id, l1tok in enumerate(l1_tokens):
        for l2id, l2tok in enumerate(l2_tokens):
            sim_score.append([list1[l1id], list2[l2id], l1tok.similarity(l2tok)])
    sim_score = list(sorted(sim_score, key=lambda comparison: comparison[2], reverse=True))
    return sim_score


def get_synset(datalist, max_syns=2):
    wn_list = []
    for word in datalist:
        try:
            synset = wn.synsets(word)
            for element_id, element in enumerate(synset):
                wn_list.append(element)
                if element_id >= max_syns:
                    break
        except:
            pass
    return set(wn_list)


def get_similarity_score(list1, list2):
    allsyns1 = set(ss for word in list1 for ss in wn.synsets(word))
    allsyns2 = set(ss for word in list2 for ss in wn.synsets(word))
    all = [(wn.wup_similarity(s1, s2) or 0, s1, s2) for s1, s2 in
           product(allsyns1, allsyns2)]
    best = max((wn.wup_similarity(s1, s2) or 0, s1, s2) for s1, s2 in product(allsyns1, allsyns2))
    return all, best


def get_sim_score(l1, l2):
    al1 = get_synset(l1)
    al2 = get_synset(l2)
    print(al1, al2)
    all_data = [(wn.wup_similarity(s1, s2) or 0, s1, s2) for s1, s2 in product(al1, al2)]
    best = max(all_data)
    return all_data, best


def get_match(list1, list2):
    all_data, best = get_sim_score(list1, list2)
    return list(sorted(all_data, key=lambda comparison: comparison[0]))
