from random import randint

try:
    from email_generator.body.bodyinfo import BodyInfo
    from email_generator.utils import match
    from email_generator.utils import utils
except Exception as exc:
    print(exc)
    from ..utils import match
    from ..utils import utils


class EmailBody(object):

    def __init__(self, myname, mycompany, myposition, our_services,
                 theirname=None, theircompany=None, theirposition=None, their_services=None,
                 our_service_details=None, datetime=None):
        self.datetime = datetime
        self.theirname = theirname
        self.theirposition = theirposition
        self.theircompany = theircompany

        self.myname = myname
        self.myposition = myposition
        self.mycompany = mycompany

        if their_services is not None:
            self.our_services, self.their_services = match.get_top_k_matches(utils.similarity(our_services, their_services))
        else:
            self.our_services = our_services
            self.their_services = None

        self.types_of_emails = ["pro", "warm"]
        self.key_info = {}
        self.selected_email_type = self.types_of_emails[randint(0, len(self.types_of_emails)-1)]
        self.body_info = BodyInfo()
        self.body, self.infobody = self.body_info.get_default_body()
        self.__create_key_info__()

    def __create_key_info__(self):

        self.key_info['{my_name}'] = self.myname
        self.key_info['{my_position}'] = self.myposition
        self.key_info['{mycompany}'] = self.mycompany
        sent = ""
        for service_id, service in enumerate(self.our_services):
            sent = sent + f"{service_id + 1}. {service}\n"
        self.key_info['{service.x}'] = sent
        self.key_info['{service.1}'] = self.our_services[0]
        self.key_info['{service.2}'] = self.our_services[1]

        self.key_info['{their_name}'] = self.theirname
        self.key_info['{theirposition}'] = self.theirposition
        self.key_info['{their_company}'] = self.theircompany
        if self.their_services is not None:
            self.key_info['{their_service}'] = self.their_services[0]
        self.key_info['{company.intro}'] = self.body_info.get_company_info(self.mycompany)
        self.key_info['{datetime}'] = self.datetime

    def populate_infobody(self):
        self.infobody['greetings'] = self.body_info.get_greeting(self.selected_email_type)
        self.infobody['introduction'] = self.body_info.get_introduction(self.selected_email_type)
        self.infobody['oration'] = self.body_info.get_body(self.selected_email_type)
        self.infobody['closing'] = self.body_info.get_closing(self.selected_email_type)

    def populate_body(self):
        for section_name, section_data in self.infobody.items():
            keys_used = section_data['keys']
            passage = section_data['content']
            for line_no, line in enumerate(passage):
                for data_key in keys_used:
                    if data_key in line:
                        line = line.replace(data_key, self.key_info[data_key])
                        passage[line_no] = line
                        continue
                line = utils.choose_options(line)
                passage[line_no] = line
            section = "".join(passage)

            fin_section = ""
            line_len_count = 1
            for char_id, char in enumerate(section):
                if char == '\n':
                    line_len_count = 0
                line_len_count += 1
                if line_len_count % 100 == 0:
                    if char != ' ' and section[char_id-1] != ' ':
                        fin_section += '-\n'
                    else:
                        fin_section += '\n'
                fin_section += char
            self.body[section_name] = fin_section

    def generate_email(self):
        self.populate_infobody()
        self.populate_body()
        email = []
        for section_name, section_info in self.body.items():
            email.append(section_info)
        text_email = "\n\n".join(email)
        return text_email
