import json
from os.path import dirname, abspath, join
from random import randint


class BodyInfo(object):
    def __init__(self):
        self.fgreetings = join(abspath(dirname(dirname(__file__))), 'data', 'greetings.json')
        self.fintroduction = join(abspath(dirname(dirname(__file__))), 'data', 'introduction.json')
        self.foration = join(abspath(dirname(dirname(__file__))), 'data', 'oration.json')
        self.fclosing = join(abspath(dirname(dirname(__file__))), 'data', 'closing.json')
        self.fcompanyinfo = join(abspath(dirname(dirname(__file__))), 'data', 'company_info.json')
        self.greetings = None
        self.introduction = None
        self.oration = None
        self.closing = None
        self.company_info = None
        self.get_data()

    def get_data(self):
        self.greetings = json.load(open(self.fgreetings, 'r'))['greetings']
        self.introduction = json.load(open(self.fintroduction, 'r'))['introduction']
        self.oration = json.load(open(self.foration, 'r'))['oration']
        self.closing = json.load(open(self.fclosing, 'r'))['closing']
        self.company_info = json.load(open(self.fcompanyinfo, 'r'))

    def get_default_body(self):
        body = {"greetings": None, "introduction": None, "oration": None, "closing": None}
        infobody = {"greetings": None, "introduction": None, "oration": None, "closing": None}
        return body, infobody

    def get_greeting(self, emailtype):
        typelen = len(self.greetings[emailtype]) - 1
        selected_greeting = self.greetings[emailtype][randint(0, typelen)]
        return selected_greeting

    def get_introduction(self, emailtype):
        typelen = len(self.introduction[emailtype]) - 1
        return self.introduction[emailtype][randint(0, typelen)]

    def get_body(self, emailtype):
        typelen = len(self.oration[emailtype]) - 1
        return self.oration[emailtype][randint(0, typelen)]

    def get_closing(self, emailtype):
        typelen = len(self.closing[emailtype]) - 1
        return self.closing[emailtype][randint(0, typelen)]

    def get_company_info(self, company_name):
        typelen = len(self.company_info[company_name]) - 1
        return self.company_info[company_name][randint(0, typelen)]
