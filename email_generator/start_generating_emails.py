from os.path import dirname, abspath, join
import pandas as pd
try:
    from email_generator.body.email_body import EmailBody
    from email_generator.utils import utils, verify_emails_utils as veu
except Exception as exc:
    from .body.email_body import EmailBody
    from .utils import utils, verify_emails_utils as veu


def get_people_from_dataframe(dataframe):
    filtered_rows = veu.filter_dataframe(dataframe)
    # print(filtered_rows.to_markdown())
    return filtered_rows


def get_unique_companies(filtered_dataframe):
    return filtered_dataframe['Company Name'].unique().tolist()


def get_company_people(people_dataframe, company_name):
    data = people_dataframe[people_dataframe['Company Name'] == company_name]
    return data


def create_email(client, my_info):
    datetime = "14/12/2020 05:00 UTC i.e. 14th of December, 2020"
    emails = client['Email']
    if emails is not None:
        if ',' in emails:
            emails = emails.split(',')
    client_name = client['Full_Name']
    if "," in client_name:
        client_name = client_name.split(',')
        client_name = list(sorted([(name, len(name)) for name in client_name], reverse=True))[0][0]
    client_company = client['Company Name']
    client_position = client['Designation']
    email_body = EmailBody(my_info['name'], my_info['company'],
                           my_info['position'], my_info['services'],
                           client_name, client_company, client_position, None,
                           my_info['service_details'], datetime=datetime)
    mail = email_body.generate_email()
    return mail, emails


def start(data_loc):
    my_name = "Mudassir"
    my_company = "Think Transportation"
    my_position = "CMO"
    our_services = ["Transportation Planning", "Traffic Engineering", "Traffic Impact Studies",
                    "Transportation Data Analytics", "Autonomous Vehicles", "Smart Cities", "Railway Engineering",
                    "Airports", "Sea Ports", "Highway Engineering and Design", "Logistics",
                    "Public Transport Planning and Design", "Traffic Survey", "Data Collection",
                    "GIS Mapping and Remote Sensing"]

    our_service_detail = []
    my_info = {
        "name": my_name,
        "company": my_company,
        "position": my_position,
        "services": our_services,
        "service_details": our_service_detail,
    }
    dataframe = pd.read_csv(data_loc)
    dataframe['Generated_Email'] = ""
    clients = get_people_from_dataframe(dataframe)
    client_companies = get_unique_companies(clients)
    people_generated_for = 1
    for company in client_companies:
        clients = get_company_people(dataframe, company)
        for index, client in clients.iterrows():
            email, email_address = create_email(client, my_info)
            print(f"{people_generated_for}\n{email_address}\n{email}\n\n")
            people_generated_for += 1
            dataframe.at[index, 'Generated_Email'] = f'"{email}"'

    return dataframe


if __name__ == '__main__':
    pd.set_option('display.max_rows', 500, 'display.max_columns', 25)
    data_loc = join(abspath(dirname(__file__)), "sample", "merger-civil-arham-sample.csv")
    generated_emails = start(data_loc)
    generated_emails.to_csv(join(abspath(dirname(__file__)), "sample", "generated_emails.csv"), index=False, mode='w')
