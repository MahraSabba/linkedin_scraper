from datetime import datetime
from os.path import dirname, abspath, join
import pandas as pd
from time import sleep
import json
try:
    from email_generator.body.email_body import EmailBody
    from email_generator.utils import utils, verify_emails_utils as veu
    from email_generator.email_sender import email_sender
except Exception as exc:
    from .body.email_body import EmailBody
    from .utils import utils, verify_emails_utils as veu
    from .email_sender import email_sender


def get_people_from_dataframe(dataframe):
    filtered_rows = veu.filter_dataframe(dataframe)
    return filtered_rows


def get_unique_companies(filtered_dataframe):
    return filtered_dataframe['Company Name'].unique().tolist()


def get_company_people(people_dataframe, company_name):
    data = people_dataframe[people_dataframe['Company Name'] == company_name]
    return data


def create_email(client, my_info):
    datetime = "14/12/2020 05:00 UTC i.e. 14th of December, 2020"
    emails = client['Email']
    if emails is not None:
        if ',' in emails:
            emails = emails.split(',')
    client_name = client['Full_Name']
    if "," in client_name:
        client_name = client_name.split(',')
        client_name = list(sorted([(name, len(name)) for name in client_name], reverse=True))[0][0]
    client_company = client['Company Name']
    client_position = client['Designation']
    email_body = EmailBody(my_info['name'], my_info['company'],
                           my_info['position'], my_info['services'],
                           client_name, client_company, client_position, None,
                           my_info['service_details'], datetime=datetime)
    mail = email_body.generate_email()
    return mail, emails


class EmailGeneratorSender(object):

    def __init__(self, dataframe_loc, my_information):
        self.email_send_limit_per_day = 200 - 20
        self.seconds_in_day = 86400
        self.email_send_diff = self.seconds_in_day/self.email_send_limit_per_day
        self.email_sender_counter = 1
        self.email_sender_datetime = datetime.now()
        self.smtpclient = email_sender.initialize_smtp_client()

        self.my_info = json.load(open(my_information, 'r'))
        self.data_loc = dataframe_loc
        self.dataframe = None

    def datetime_checker(self):
        if self.email_sender_counter == self.email_send_limit_per_day:
            if (self.email_sender_datetime - datetime.now()).total_seconds() >= self.seconds_in_day:
                self.email_sender_counter = 1
                self.email_sender_datetime = datetime.now()
            else:
                time_to_sleep = self.seconds_in_day - (self.email_sender_datetime - datetime.now()).total_seconds()
                sleep(time_to_sleep)

    def get_people(self):
        self.dataframe = pd.read_csv(self.data_loc)
        clients = get_people_from_dataframe(self.dataframe)
        client_companies = get_unique_companies(clients)
        return client_companies

    def generate_emails(self):
        client_companies = self.get_people()
        people_generated_for = 1
        for company in client_companies:
            self.datetime_checker()
            clients = get_company_people(self.dataframe, company)
            for index, client in clients.iterrows():
                email, email_address = create_email(client, self.my_info)
                people_generated_for += 1
                self.dataframe.at[index, 'Content_Generated'] = f'"{email}"'
                client['Content_Generated'] = f'"{email}"'
                email_sent, client, message = email_sender.send_one_email(self.smtpclient,
                                                                          self.my_info['email'],
                                                                          client)
                print(email_sent, message)
                self.email_sender_counter += 1
                print(f"Sleeping for {self.email_send_diff} seconds........ Message sent == {email_sent}. ")
                if email_sent:
                    self.dataframe.at[index, 'Email_Sent'] = True
                sleep(self.email_send_diff)
                self.dataframe.to_csv(self.data_loc, index=False, mode='w')
                break
            break


if __name__ == '__main__':
    # pd.set_option('display.max_rows', 500, 'display.max_columns', 25)
    data_loc = join(abspath(dirname(dirname(__file__))), "company_lists", "merger-civil-mexico.csv")
    my_information_path = join(abspath(dirname(dirname(__file__))), "config", "my_information.json")

    generator = EmailGeneratorSender(data_loc, my_information_path)
    while True:
        try:
            generator.generate_emails()
        except:
            pass