from os.path import join, abspath, dirname
import subprocess
import time


def refresh_ip():
    path = join(abspath(dirname(__file__)), "Tor Browser", "Browser", "TorBrowser", "Tor")

    try:
        subprocess.Popen(['Taskkill','/IM','tor.exe','/F'])
    except:
        pass
    time.sleep(2)
    try:
        subprocess.Popen(['cmd', '/c', 'tor.exe'],cwd=path,shell=True)
    except:
        pass
    print("ip - changed")





