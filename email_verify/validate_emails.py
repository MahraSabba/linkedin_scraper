import sys
from os.path import dirname, abspath, join, exists
import requests
from bs4 import BeautifulSoup
from multiprocessing import Pool
import pandas as pd
try:
    from email_verify.refresh_ip import refresh_ip
    from scripts import verify_emails_utils as veu
except Exception as exc:
    from .refresh_ip import refresh_ip
    sys.path.append(abspath(dirname(dirname(__file__))))
    from scripts import verify_emails_utils as veu


session = requests.Session()


def verify_email_existence(email_index):
    email, index = email_index
    print(f"Email : {email}, Index: {index}")
    while True:
        url = 'https://validateemailaddress.org/'

        querystring = {"email": email}

        proxy = '127.0.0.1:9050'
        proxy1 = {'http': f'socks5h://{proxy}', 'https': f'socks5h://{proxy}'}

        cookie = {
            'Cookie': '__gads=ID=56dfd13a8600c459-2228222446a60028:T=1602625834:RT=1602625834:S=ALNI_'
                      'MZlR9ZMDSZ1tfsiR5CA3jEK5nuRiQ'
        }
        cookie = {'consent': "2020-10-15T20%3A53%3A42.200Z"}

        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/'
                      'apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-GB,en;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'Content-Length': '29',
            'Content-Type': 'application/x-www-form-urlencoded',

            # 'Host': 'validateemailaddress.org',
            'Origin': 'https://validateemailaddress.org',
            'Referer': 'https://validateemailaddress.org/',
            'sec-ch-ua': '"\\Not;A\"Brand";v="99", "Google Chrome";v="85", "Chromium";v="85"',
            'sec-ch-ua-mobile': '?0',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/85.0.4183.121 Safari/537.'
        }
        try:
            response = session.post(url, proxies=proxy1, headers=headers, data=querystring)
            html = response.text

            find_html = BeautifulSoup(html, features="lxml")
            mydivs = find_html.findAll("div", {"class": "success valid"})
            if "too many requests" not in response.text:
                response = 0
                if len(mydivs) > 0:
                    response = True
                    break
                else:
                    response = False
                    break
            else:
                refresh_ip()
        except Exception as exc:
            print(exc)
            continue
    print(email, response, index)
    return [email, response, index]


def single(email_tuple):
    validated_emails = []
    for email, index in email_tuple:
        email, response, index = verify_email_existence((email, index))
        if response is None:
            pass
        if type(response) is not bool:
            pass
        if response:
            validated_emails.append(email)
    return validated_emails




def multi(pollnum, fun, tuple_list):
    p = Pool(pollnum)
    q = p.map(fun, tuple_list)
    p.close()
    p.terminate()
    p.join()
    return q


def read_email_address(list_file):
    with open(list_file, 'r', encoding='utf-8') as listfile:
        readlines = listfile.readlines()
        emails = {}
        for line in readlines:
            if ":" in line:
                email = (line.split(":"))[0]
                emails[email] = 1
            else:
                emails[line] = 1
    emails = list(emails)
    print(len(emails))
    return emails


def process_verified_emails(dataframe, existence_list):
    email_index = dataframe.columns.get_loc('Email')
    for email, response, index in existence_list:
        if response is None:
            continue
        if type(response) is not bool:
            continue
        if response:
            if pd.isna(dataframe.iat[index, email_index]):
                dataframe.iat[index, email_index] = email
            else:
                dataframe.iat[index, email_index] = f"{dataframe.iat[index, email_index]},{email}"
    return dataframe


def start_process(main_dataframe):
    people_data = veu.get_scraped_not_sent_people(main_dataframe)
    email_index_tuple = veu.create_tuples_of_emails(people_data)
    main_dataframe = veu.set_emails_to_none(main_dataframe, people_data)
    refresh_ip()
    existence_list = multi(5, verify_email_existence, email_index_tuple)
    main_dataframe = process_verified_emails(main_dataframe, existence_list)
    return main_dataframe


def validate_emails(info, saving_location=None):
    if saving_location is None:
        saving_location = join(abspath(dirname(dirname(__file__))), 'company_lists',
                          'verified_people_emails.csv')
    dataframe = None
    if type(info) is str:
        if exists(info):
            dataframe = start_process(pd.read_csv(info))
    if type(info) is pd.DataFrame:
        dataframe = start_process(info)
    dataframe.to_csv(saving_location, index=False)
    exit(-1)


def init_process(datafile_location):
    validate_emails(datafile_location)


if __name__ == '__main__':
    data_loc = join(abspath(dirname(dirname(__file__))), 'company_lists', 'merger-civil-arham-sample.csv')
    validate_emails(data_loc)
