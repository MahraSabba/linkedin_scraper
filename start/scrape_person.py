import os
import sys
import json
from os.path import dirname, join, abspath, exists
from selenium import webdriver
import pandas as pd

BASEPATH = abspath(dirname(dirname(__file__)))
try:
    from linkedin_scraper import Person, Company, actions, Information
except Exception as exc:
    print(exc)
    sys.path.append(BASEPATH)
    from linkedin_scraper import Person, Company, actions, Information


def get_credentials():
    configuration_path = join(dirname(dirname(__file__)),
                              "config", "config.json")
    if not exists(abspath(dirname(configuration_path))):
        os.makedirs(abspath(dirname(configuration_path)), exist_ok=True)
        dummy_js = {'credentials': [[]]}
        json.dump(dummy_js, open(configuration_path, 'w'))
        print(f"Add credentials to the configuration at : \n{configuration_path}\nin the following syntax: [[email, "
              f"password], [email, password]...]")
        exit()
    config_data = json.load(open(configuration_path, 'r'))
    config_creds = config_data['credentials']
    credentials = [(c[0], c[1]) for c in config_creds]
    return credentials


def parse_csv(csv_file_loc):
    companies = []
    with open(csv_file_loc, 'r', encoding='utf-8') as csv_file:
        for line in csv_file.readlines():
            if "," in line:
                line = line.split(',')
                link = line[0]
                domain = line[1]
            else:
                link = line.replace('\n', '').replace('\ufeff', '')
                domain = " "
            companies.append([f"https://www.{link}", domain])
    return companies


def get_csv_file():
    folder = join(dirname(dirname(__file__)), 'company_lists')
    files = [join(folder, file) for file in os.listdir(folder) if '.csv' in file]
    return files


def parse_csv_pd(file):
    return pd.read_csv(file)


def get_all_companies():
    companies = None
    file_location = None
    files = get_csv_file()
    for file in files:
        if companies is None:
            companies = parse_csv_pd(file)
            file_location = file
        else:
            com = parse_csv_pd(file)
            companies.append(com)
            file_location = file
    return companies, file_location


if __name__ == "__main__":
    # Add pairs/tuples of (email, password) combinations to the data.
    companies, file_location = get_all_companies()
    credentials = get_credentials()
    driver_path = join(dirname(dirname(__file__)),
                       'chromedriver_win32',
                       'chromedriver.exe')
    information = Information.CompanyScraper(credentials=credentials,
                                             driver_path=driver_path,
                                             company_dataframe=companies,
                                             saving_location=file_location)
    information.start_scraping()
