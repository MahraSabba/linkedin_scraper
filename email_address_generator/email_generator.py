from os.path import abspath, dirname, join, basename
import sys
from re import sub
import string
import pandas as pd
import jellyfish

BASEPATH = abspath(dirname(dirname(__file__)))
sys.path.append(BASEPATH)


def match_company_and_domain(company, domain_data):
    similarities = []
    for id, domain in enumerate(domain_data['display_url']):
        if type(domain) is not str:
            continue
        if company is None:
            return None
        domain_x = domain.split('.')[0]
        sim1 = jellyfish.jaro_similarity(company, domain_x)
        sim2 = jellyfish.jaro_similarity(company, domain_data['linkedin'][id].split('/')[-1].replace('-', ''))
        similarities.append([domain, sim1])
        similarities.append([domain, sim2])
    sim_list = list(sorted(similarities, key=lambda val: val[1], reverse=True))
    if sim_list[0][1] > 0.80:
        return sim_list[0][0]
    for id, domain in enumerate(domain_data['display_url']):
        if " " in company:
            company = company.split(' ')
        name = domain_data['linkedin'][id].split('/')[-1].replace('-', ' ').split(' ')
        if type(company) is list:
            for cpart in company:
                for dpart in name:
                    if cpart == dpart:
                        return domain
        elif type(company) is str:
            for dpart in name:
                if company == dpart:
                    return domain
        return None


def get_position(exp, headline, company_name_main):

    exp = exp.split(';')[0]
    position = exp.split(' at ')[0]
    if "Company Name\\n" in position:
        company_name = position.replace("Company Name\\n", "")
        try:
            position = exp[1].split("'")[0]
            if "," in company_name:
                position = company_name.split(',')[0]
        except:
            pass
    position = position.lower()
    position = sub("internship|full time|full-time|self employed|part-time|part time|self-employed|freelancer", "", position)
    similarity = jellyfish.jaro_similarity(company_name_main, position)
    if similarity > 0.8:
        if " at " in headline:
            position = headline.split(' at ')[0]
    return position


def get_company(exp):
    exp = exp.split("b'")[1:]
    position = exp[0].replace("' at ", "")
    company_name = None
    if "Company Name\\n" in position:
        company_name = position.replace("Company Name\\n", "")
        try:
            position = exp[1].split("'")[0]
            if "," in company_name:
                position = company_name.split(',')[0]
        except:
            pass
    else:
        try:
            company_name = exp[1].split("'")[0]
            if "," in company_name:
                company_name = company_name.split(',')[0]
        except:
            pass
    if company_name is None:
        return None
    if "," in company_name:
        company_name = company_name.split(',')[0]
    company_name = sub("internship|full time|full-time|self employed|part-time|part time|self-employed|freelancer", "", company_name.lower())
    return company_name


def match_domain(employee_data, domain_data):

    employee_data['Domain'] = employee_data['CurrentCompany'].apply(
        lambda company: match_company_and_domain(company, domain_data))

    total_employees = len(employee_data['Domain'])
    for id in range(1, total_employees-2, 1):
        above = employee_data['Domain'][id-1]
        current = employee_data['Domain'][id]
        below = employee_data['Domain'][id+1]
        if current is not None:
            continue
        if current != above and current != below:
            employee_data['Domain'][id] = above
        employee_data['Domain'][id] = above

    for id, emp in enumerate(employee_data['Domain']):
        print(employee_data['CurrentCompany'][id], emp)
    return employee_data


def create_emails(employee_data, email_csv_path):
    domains_used = set()
    with open(email_csv_path, 'w', encoding='utf-8') as email_csv:
        for id, employee_name_str in enumerate(employee_data['EmployeeName']):
            domain = employee_data['Domain'][id]
            domains_used.add(domain)
            if ' ' in employee_name_str:
                employee_name_lst = employee_name_str.split(' ')
                if len(employee_name_lst) >= 2:
                    fname = employee_name_lst[0]
                    lname = employee_name_lst[-1]
                    email_csv.write(f'{fname}@{domain}\n')
                    email_csv.write(f'{fname}.{lname}@{domain}\n')
                    email_csv.write(f'{fname}{lname}@{domain}\n')
                else:
                    fname = employee_name_lst[0]
                    email_csv.write(f'{fname}@{domain}\n')
        for dom in list(domains_used):
            email_csv.write(f"hello@{dom}\n")
            email_csv.write(f"ceo@{dom}\n")
            email_csv.write(f"info@{dom}\n")


def main():
    employee_data_filepath = join(BASEPATH, 'done', 'employees.csv')
    domain_data_filepath = join(BASEPATH, 'company_lists', 'linkedin pages-civil-with-domain.txt')
    email_list_filepath = join(BASEPATH, 'company_lists', 'emails.txt')
    employee_data = pd.read_csv(employee_data_filepath, encoding='utf-8')
    domain_data = pd.read_csv(domain_data_filepath, encoding='utf-8')
    pd.set_option("display.max_rows", 100, "display.max_columns", 100)
    employee_data['EmployeeName'] = employee_data['EmployeeName'].apply(
        lambda employee: sub('\w+\.\w+|\w+\.|pe|engineer|meng|beng', '', employee.split(',')[0].lower()))
    employee_data['CurrentPosition'] = employee_data['Experience'].apply(
        lambda exp: get_position(exp))
    employee_data['CurrentCompany'] = employee_data['Experience'].apply(
        lambda exp: get_company(exp))
    employee_data = match_domain(employee_data, domain_data)
    create_emails(employee_data, email_list_filepath)


if __name__ == '__main__':
    main()