import os
from os.path import dirname, abspath, join, exists
from linkedin_scraper import Person, Company, actions
from selenium import webdriver
import numpy as np
from random import randint
import pandas as pd
import time
from datetime import datetime
try:
    from email_address_generator import email_generator as eg
    import email_verify.validate_emails as valemails
    import email_generator.start_generating_emails as sge
except:
    import sys
    sys.path.append(abspath(dirname(dirname(__file__))))
    from email_address_generator import email_generator as eg
    import email_verify.validate_emails as valemails
    import email_generator.start_generating_emails as sge

BASEPATH = abspath(dirname(dirname(__file__)))


def convert_to_bool(value):
    if type(value) is None:
        return False
    if type(value) is np.nan:
        return False
    if type(value) is str:
        if value == 'FALSE':
            return False
        if value == "TRUE":
            return True
    if type(value) is bool:
        return value


class CompanyScraper(object):
    def __init__(self, credentials=None, company_dataframe=None, driver_path=None, saving_location=None,
                 max_employees=6, save_interval=10, wait_time_after_login=20, refresh_interval=18):
        self.ip_refresh_counter = 0
        self.refresh_ip_after = refresh_interval

        self.company_df_keys = []
        self.saving_folder = join(abspath(dirname(dirname(__file__))), "data")
        self.wait_time_after_login = wait_time_after_login
        if not os.path.exists(self.saving_folder):
            os.makedirs(self.saving_folder, exist_ok=True)
        if credentials is None:
            exit("No credentials are given.. Cannot proceed without credentials.")
        self.credentials = credentials
        self.used_credentials = []
        if company_dataframe is None:
            company_dataframe = []
        self.original_dataframe = company_dataframe.copy(deep=True)
        self.company_dataframe = company_dataframe
        self.company_dataframe_columns = self.company_dataframe.keys().tolist()
        self.dataframe_usable_keys = {"id": "id",
                                      "company_name": "Company Name",
                                      "domain": "Website",
                                      "linkedin_url": "linkedin",
                                      "scraped": "Is_Scraped",
                                      "full_name": "Full_Name",
                                      "fname": "First_Name",
                                      "lname": "Last_Name",
                                      "headline": "Headline",
                                      "designation": "Designation",
                                      "experience": "Experience",
                                      "education": "Education",
                                      "emails": "Email",
                                      "email_ver": "Email_Verified",
                                      "content_gen": "Content_Generated",
                                      "email_sent": "Email_Sent"
                                      }
        self.dataframe_filter_keys = {
            "industry": "Industry",
            "country": "Country",
            "founded": "Founded",
            "size": "size_range",
            "employees": "Number of employees"
        }
        self.clean_dataframe()
        self.filter_dataframe()
        self.companies = self.get_unique_unscraped_companies()
        self.saving_location = saving_location
        self.max_employees = max_employees
        self.save_interval = save_interval
        chrome_profile_path = join(BASEPATH, "chromedriver_win32", "Profile")
        if not exists(chrome_profile_path):
            os.mkdir(chrome_profile_path)
        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")
        options.add_argument(f"user-data-dir={chrome_profile_path}")
        self.driver = webdriver.Chrome(driver_path, options=options)

        no_of_accounts = len(self.credentials)
        print(f"Number of accounts to use are: {no_of_accounts}")
        try:
            self.login()
        except:
            pass

    def filter_dataframe(self):
        return
        # self.dataframe_filters = [
        #     # f"'201 - 500' == {self.dataframe_filter_keys['size']}",
        #     f'{self.dataframe_filter_keys["country"]}.str.contains("united")']
        # for df_filter in self.dataframe_filters:
        #     self.company_dataframe = self.company_dataframe.query(df_filter, engine='python')

    def clean_dataframe(self):
        self.company_dataframe.fillna(' ', inplace=True)
        self.company_dataframe[self.dataframe_usable_keys['scraped']].apply(
            lambda scraped: convert_to_bool(scraped))
        self.company_dataframe[self.dataframe_usable_keys['email_ver']].apply(
            lambda ver: convert_to_bool(ver))
        self.company_dataframe[self.dataframe_usable_keys['content_gen']].apply(
            lambda content: convert_to_bool(content))
        self.company_dataframe[self.dataframe_usable_keys['email_sent']].apply(
            lambda esent: convert_to_bool(esent))

    def get_unique_unscraped_companies(self):
        company_df = self.company_dataframe[self.company_dataframe[self.dataframe_usable_keys['scraped']] == False]
        return company_df[self.dataframe_usable_keys['company_name']].unique().tolist()

    def login(self):
        if len(self.credentials) > len(self.used_credentials):
            index_to_use = len(self.used_credentials)
        else:
            index_to_use = len(self.used_credentials) % len(self.credentials)

        email, password = self.credentials[index_to_use]
        if len(password) > 1 and len(email) > 4:
            actions.login(self.driver, email, password)

        time.sleep(self.wait_time_after_login)

    def generate_emails(self, employee_name_str, domain, email=None):
        emails = []
        if ' ' in employee_name_str:
            if ',' in employee_name_str:
                employee_name_str = employee_name_str.split(',')[0]
            employee_name_lst = employee_name_str.lower().split(' ')
            if len(employee_name_lst) >= 2:
                fname = employee_name_lst[0]
                lname = employee_name_lst[-1]
                emails.append(f'{fname}@{domain}')
                emails.append(f'{fname}.{lname}@{domain}')
                emails.append(f'{fname}{lname}@{domain}')
            else:
                fname = employee_name_lst[0]
                lname = ""
                emails.append(f'{fname}@{domain}\n')
        else:
            fname = employee_name_str
            lname = ''
            emails.append(f'{fname}@{domain}\n')
        if email is not None:
            emails.append(email)
        return emails, fname, lname

    def create_default_email_row(self, company_row, domain):
        if domain is not None:
            if domain != ' ' or domain != '':
                emails = f"info@{domain},ceo@{domain},hello@{domain}"
        else:
            emails = None
        new_row = self.create_new_row(company_row, emails=emails)

        self.original_dataframe = self.original_dataframe.append(new_row, ignore_index=True)
        self.original_dataframe.reset_index(drop=True)

    def verify_email_per_row(self, emails):

        def convert_to_list(email_str):
            print(email_str)
            if "," in email_str:
                email_list = [(email, 1) for email in email_str.split(',')]
            else:
                email_list = [(email_str, 1)]
            print(email_list)
            return email_list

        email_list = None

        if emails is None:
            return None

        if type(emails) is list:
            if emails is []:
                return None
            else:
                email_list = emails
        elif type(emails) is str:
            email_list = convert_to_list(emails)
        if self.ip_refresh_counter % self.refresh_ip_after == 0:
            valemails.refresh_ip()
        self.ip_refresh_counter += 1
        existing_email_list = valemails.single(email_list)
        if existing_email_list is []:
            return None
        emails = ",".join(existing_email_list)
        return emails

    def start_scraping(self):
        for company in self.companies:
            company_row = self.company_dataframe[self.company_dataframe[self.dataframe_usable_keys['company_name']]
                                                 == company].iloc[0]

            linkedin_url = f"https://www.{company_row[self.dataframe_usable_keys['linkedin_url']]}"
            domain = company_row[self.dataframe_usable_keys['domain']]
            try:
                print(linkedin_url)
                company = Company(linkedin_url, driver=self.driver, close_on_complete=False,
                                  max_employees=self.max_employees)
                total_employees_gathered = len(company.employees)
                company_name = company_row[self.dataframe_usable_keys['company_name']]
                processed_employees = 0
                for employee_id in range(0, total_employees_gathered):
                    processed = company.scrape_employee(employee_id)
                    if processed is not None:
                        while not processed:
                            self.login()
                            processed = company.scrape_employee(employee_id)
                    employee = company.employees[employee_id]
                    if employee is None:
                        continue
                    processed_employees += 1
                    if processed_employees >= self.max_employees:
                        break
                    exp = "" if employee.experiences is None else \
                        ";".join([str(exp) for exp in employee.experiences])
                    headline = "" if employee.current_headline is None else \
                        str(employee.current_headline)
                    education = "" if employee.educations is None else ";".join(
                        [str(edu) for edu in employee.educations])
                    email = "" if employee.email is None else \
                        str(employee.email)
                    fullname = str(employee.name)
                    position = eg.get_position(exp, headline, company_name)
                    if email != "":
                        emails, fname, lname = self.generate_emails(fullname, domain, email)
                    else:
                        emails, fname, lname = self.generate_emails(fullname, domain, None)
                    new_row = self.create_new_row(company_row, fullname, fname, lname,
                                                  headline, position, exp, education, emails)
                    self.original_dataframe = self.original_dataframe.append(new_row, ignore_index=True)
                    self.original_dataframe.reset_index(drop=True)
                    if processed_employees % self.save_interval == 0:
                        self.save_pandas_dataframe(company_name=company_name)
                self.create_default_email_row(company_row, domain)
                self.save_pandas_dataframe(company_name=company_name)
                time.sleep(randint(18, 25))
            except Exception as exc:
                self.create_default_email_row(company_row, domain)
                company_name = company_row[self.dataframe_usable_keys['company_name']]
                self.save_pandas_dataframe(company_name=company_name)
                print("LOG:: ", exc)
                print(f"{linkedin_url} does not exist")

    def create_new_row(self, company_row, fullname=None, fname=None, lname=None, headline=None, position=None,
                       exp=None, education=None, emails=None):
        email_verified = False
        if headline is not None:
            headline = headline
        if position is not None:
            position = position
        if exp is not None:
            exp = exp
        if education is not None:
            education = education
        if emails is not None:
            if type(emails) is list:
                emails = ",".join(emails)
            emails = self.verify_email_per_row(emails)
            email_verified = True

        new_row = {
            self.dataframe_usable_keys['id']: len(self.original_dataframe) + 1,
            'url': company_row['url'],
            self.dataframe_usable_keys['company_name']: company_row[self.dataframe_usable_keys['company_name']],
            self.dataframe_usable_keys['domain']: company_row[
                self.dataframe_usable_keys['domain']],
            'Snippet': company_row['Snippet'],
            'Founded': company_row['Founded'],
            "Industry": company_row["Industry"],
            self.dataframe_usable_keys['linkedin_url']: company_row[
                self.dataframe_usable_keys['linkedin_url']],
            'size_range': company_row['size_range'],
            "display_url": company_row['display_url'],
            "Number of employees": company_row["Number of employees"],
            "Country": company_row['Country'],
            "Headquarters": company_row['Headquarters'],
            self.dataframe_usable_keys['scraped']: True,
            self.dataframe_usable_keys['full_name']: fullname,
            self.dataframe_usable_keys['fname']: fname,
            self.dataframe_usable_keys['lname']: lname,
            self.dataframe_usable_keys['headline']: headline,
            self.dataframe_usable_keys['designation']: position,
            self.dataframe_usable_keys['experience']: exp,
            self.dataframe_usable_keys['education']: education,
            self.dataframe_usable_keys['emails']: emails,
            self.dataframe_usable_keys['email_ver']: email_verified,
            self.dataframe_usable_keys['content_gen']: False,
            self.dataframe_usable_keys['email_sent']: False,
        }
        return new_row

    def save_pandas_dataframe(self, company_name):
        droppable_frames = self.original_dataframe[
                    (self.original_dataframe[self.dataframe_usable_keys['company_name']] == company_name)
                    & (self.original_dataframe[self.dataframe_usable_keys['scraped']] == False)
                    ]
        drop_frames_index = droppable_frames.index.astype(int)[0]
        self.original_dataframe = self.original_dataframe.drop([drop_frames_index])
        self.original_dataframe.dropna()
        self.original_dataframe.reset_index(drop=True)
        self.original_dataframe.to_csv(self.saving_location, index=False)
