

def convert_to_str(value):
    try:
        if value is not None:
            if type(value) is str:
                if "b\'" in value:
                    value = value.replace("b\'", "")
                if "\'" in value:
                    value = value.replace("\'", "")
                return value
            if type(value) is bytearray:
                return value.decode('utf-8')
        return None
    except:
        return value


class Institution(object):
    institution_name = None
    website = None
    industry = None
    type = None
    headquarters = None
    company_size = None
    founded = None

    def __init__(self, name=None, website=None, industry=None, type=None, headquarters=None, company_size=None, founded=None):
        self.name = convert_to_str(name)
        self.website = convert_to_str(website)
        self.industry = convert_to_str(industry)
        self.type = convert_to_str(type)
        self.headquarters = convert_to_str(headquarters)
        self.company_size = convert_to_str(company_size)
        self.founded = convert_to_str(founded)


class Experience(Institution):
    from_date = None
    to_date = None
    description = None
    position_title = None
    duration = None

    def __init__(self, from_date = None, to_date = None, description = None, position_title = None, duration = None, location = None):
        self.from_date = convert_to_str(from_date)
        self.to_date = convert_to_str(to_date)
        self.description = convert_to_str(description)
        self.position_title = convert_to_str(position_title)
        self.duration = convert_to_str(duration)
        self.location = convert_to_str(location)

    def __repr__(self):
        return f"{self.position_title} at {self.institution_name} from {self.from_date} to {self.to_date} for {self.duration} based at {self.location}"

class Education(Institution):
    from_date = None
    to_date = None
    description = None
    degree = None

    def __init__(self, from_date = None, to_date = None, description = None, degree = None):
        self.from_date = convert_to_str(from_date)
        self.to_date = convert_to_str(to_date)
        self.description = convert_to_str(description)
        self.degree = convert_to_str(degree)

    def __repr__(self):
        return "{degree} at {company} from {from_date} to {to_date}".format( from_date = self.from_date, to_date = self.to_date, degree = self.degree, company = self.institution_name)


class Interest(Institution):
    title = None
    
    def __init__(self, title = None):
        self.title = convert_to_str(title)
    
    def __repr__(self):
        return self.title


class Accomplishment(Institution):
    category = None
    title = None
    
    def __init__(self, category = None, title = None):
        self.category = convert_to_str(category)
        self.title = convert_to_str(title)
    
    def __repr__(self):
        return self.category + ": " + self.title


class Scraper(object):
    driver = None

    def is_signed_in(self):
        try:
            self.driver.find_element_by_id("profile-nav-item")
            return True
        except:
            pass
        return False

    def __find_element_by_class_name__(self, class_name):
        try:
            self.driver.find_element_by_class_name(class_name)
            return True
        except:
            pass
        return False

    def __find_element_by_xpath__(self, tag_name):
        try:
            self.driver.find_element_by_xpath(tag_name)
            return True
        except:
            pass
        return False

    def __find_enabled_element_by_xpath__(self, tag_name):
        try:
            elem = self.driver.find_element_by_xpath(tag_name)
            return elem.is_enabled()
        except:
            pass
        return False

    def __find_first_available_element__(self, *args):
        for elem in args:
            if elem:
                return elem[0]

